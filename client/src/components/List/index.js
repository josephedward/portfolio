import React from "react";
import "./style.css";
import { useEffect } from "react";

export function List({ children }) {

  const [width, setWidth] = React.useState(window.innerWidth);
  const breakpoint = 800;

  useEffect(() => {
    window.addEventListener("resize", () => setWidth(window.innerWidth));
  }, []);

  return width > breakpoint ?(
      <ul className="list-group"
      width="75%"
      >{children}</ul>
  ):(
    <ul className="list-group"
    width="100%"
    >{children}</ul>
  )
}

export function ListItem({ children }) {

  const [width, setWidth] = React.useState(window.innerWidth);
  const breakpoint = 800;

  useEffect(() => {
    window.addEventListener("resize", () => setWidth(window.innerWidth));
  }, []);

  return width > breakpoint ? (
  <li  
  width="75%"
  style={mAlign}
   className="list-group-item
   ">{children}</li>
   ) : (
<li  
  width="100%"
  style={{...mAlign, width: "100%"}}
   className="list-group-item
   ">{children}</li>
   );
}


const mAlign={
  fontSize:"15px",
  width:"75%",
  margin: "40px auto",
  textAlign:"center",
  "align-items": "center"
}
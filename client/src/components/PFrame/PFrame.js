import React from "react";
import { ListItem } from "../List";

export default function PFrame(props) {  
  return (
      <ListItem 
      
      key={props.id}>
        <strong>
          <a href={props.html_url}>{props.name}</a>
        </strong>
        <br />
        <a style={{color:"orange",
        overflowWrap: "break-word",
        wordBreak:"break-all"
      }} href={props.homepage}>
          {props.homepage}
        </a>
        <p>{props.description}</p>
        {props.homepage ? (
          <iframe
            width="100%"
            height="800"
            title="Deployment Iframe"
            src={props.homepage}
          />
        ) : (
          <div>
            {/* <p>Your browser does not support iframes.</p> */}
          </div>
        )}
      </ListItem>
  );
}



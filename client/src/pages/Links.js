import React, { Component } from "react";

class Links extends Component {
  render() {
    return (
      <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
      }}
    >
        <iframe
        style={{"width": "100%", "height": "100vh", display: "flex"}}
          title="SingleLink Links"
          src="https://singlelink-production-bbcd.up.railway.app"
          
        />
      </div>
    );
  }
}

export default Links;

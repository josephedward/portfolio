import React, { Component } from "react";
import API from "../utils/API";
import { Col, Row, Container } from "../components/Grid";
import { List } from "../components/List";
import PFrame from "../components/PFrame/PFrame.js";
import testProjects from "../test/projects.json";
class Projects extends Component {
  state = {
    projects: [],
    title: "",
    contributors: "",
    description: "",
  };

  componentDidMount() {
    this.loadprojects();
  }

  loadprojects = () => {
    if (process.env.NODE_ENV === "development") {
      this.setState({
        projects: testProjects,
      });
    } else {
      API.getrepos()
        .then((res) =>
          // console.log(res.data),
          this.setState({
            projects: res.data,
            title: "",
            contributors: "",
            description: "",
          })
        )
        .catch((err) => {
          console.log(err);
        });
    }
  };

  requestOptions = {
    method: "GET",
    headers: {
      "User-Agent": "PostmanRuntime/7.21.0",
      Accept: "*/*",
      "Cache-Control": "no-cache",
      "Postman-Token": "9d4b0799-97d2-4f8b-945c-e31f72b4361e",
      "Accept-Encoding": "gzip, deflate",
      Connection: "keep-alive",
      "Access-Control-Allow-Origin": true,
    },
    redirect: "follow",
  };

  render() {
    return (
      <div>
        <Container fluid>
          <Row>
            <Col size="lg">
              <h3
                style={{
                  background: "black",
                  color: "white",
                }}
              >
                Visit deployment URL and disable mixed-content blocking for full
                functionality.
              </h3>
              {this.state.projects.length ? (
                <List>
                  {this.state.projects.map((project) => (
                    <PFrame
                      id={project.id}
                      name={project.name}
                      html_url={project.html_url}
                      homepage={project.homepage}
                      description={project.description}
                    />
                  ))}
                </List>
              ) : (
                <h3 style={{ background: "white" }}>No Results to Display</h3>
              )}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Projects;

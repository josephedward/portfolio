import React from "react";
import { useEffect } from "react";

function Home() {
  const [width, setWidth] = React.useState(window.innerWidth);
  const breakpoint = 800;

  useEffect(() => {
    window.addEventListener("resize", () => setWidth(window.innerWidth));
  }, []);


  return width > breakpoint ? (
    <iframe
      style={{ width: "100%", height: "100vh", display: "flex" }}
      title="Joe's Old Site"
      src= {process.env.NODE_ENV === "development"? "http://127.0.0.1:5500/Documents/GitHub/sci-fi_personal/index.html":"https://josephedward.github.io/sci-fi_portfolio/"}
      
    />
  ) : (
    <div />
  );
}

export default Home;

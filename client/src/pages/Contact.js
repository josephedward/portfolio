import React, { Component } from "react";

class Contact extends Component {
  render() {
    return (
      <div>
        <iframe
        style={{"width": "100%", "height": "100vh", display: "flex"}}
          title="Calendly Scheduling"
          src="https://calendly.com/josephedwardwork"
        />
      </div>
    );
  }
}

export default Contact;

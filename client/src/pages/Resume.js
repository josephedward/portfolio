import React, { Component } from "react";

class Resume extends Component {
  render() {
    return (
      <div
      style={{backgroundColor:"white"}}
      >
        <iframe
        style={{"width": "100%", "height": "100vh", display: "flex"}}
          title="Google Docs Resume"
          src="https://docs.google.com/document/d/16E-rsdBbb_p6L_jDQ9TIsMZWTcga6u90Nl9I6aMQdfI/edit?usp=sharing"
        />
      </div>
    );
  }
}

export default Resume;
